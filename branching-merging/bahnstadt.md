# Bahnstadt

### Balthazar

Schwetzinger Terrase 2, 69115 Heidelberg

https://www.balthazar-bahnstadt.de

#### Pro

- diverse cuisine
- cosy and chilled
- good service

#### Con

- not many vegetarian/vegan options
- quite popular so could be busy
